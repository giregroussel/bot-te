function addScript(src) {
  const script = document.createElement("script");
  script.src = src;
  document.body.appendChild(script);
}

function addMessenger() {
  const div = document.createElement("div");
  div.innerHTML = ` <df-messenger\n
    intent="WELCOME"\n
    chat-icon="https://cdn.discordapp.com/attachments/828970103474749452/862330777445990400/171252215_796842947603708_6440210082190996495_n.png"\n
    chat-title="Bot té" \n
    agent-id="57ed3011-798b-4601-ba7a-3a5227e7daae"\n
    language-code="fr"\n
    ></df-messenger>\n
    <style>
    df-messenger {
     --df-messenger-bot-message: #449BD2;
     --df-messenger-button-titlebar-color: #3479C4;
     --df-messenger-chat-background-color: #fafafa;
     --df-messenger-font-color: white;
     --df-messenger-send-icon: #449BD2;
     --df-messenger-user-message: #b8b8b8;
    }
  </style> `;
  div.id = "chatbot";
  document.body.appendChild(div);
}

addMessenger();
addScript(
  "https://www.gstatic.com/dialogflow-console/fast/messenger/bootstrap.js?v=1"
);
