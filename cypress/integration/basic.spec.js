/// <reference types="cypress" />

describe("Welcome", () => {
  beforeEach(() => {
    cy.visit("https://www.doctolib.fr/");

    cy.window().then((win) => {
      const script = win.document.createElement("script");
      script.src =
        "https://www.gstatic.com/dialogflow-console/fast/messenger/bootstrap.js?v=1";
      win.document.body.appendChild(script);

      const div = win.document.createElement("div");
      div.innerHTML =
        '<df-messenger intent="WELCOME" chat-title="doctolib" agent-id="57ed3011-798b-4601-ba7a-3a5227e7daae" language-code="fr"  ></df-messenger>';
      div.id = "chatbot";
      win.document.body.appendChild(div);
    });

    cy.get("#didomi-notice-disagree-button").click();
  });

  it("exists", () => {
    cy.get("#chatbot").should("exist");
  });

  it("opens", () => {
    cy.get(".message-list-wrapper", { includeShadowDom: true }).should("exist");
    cy.get(".message-list-wrapper", { includeShadowDom: true }).click();
  });

  it("close", () => {
    cy.get(".message-list-wrapper", { includeShadowDom: true }).should("exist");
    cy.get(".message-list-wrapper", { includeShadowDom: true }).click();

    cy.get("#widgetIcon", { includeShadowDom: true }).should("exist");
    cy.get("#widgetIcon", { includeShadowDom: true }).click();
  });
});
