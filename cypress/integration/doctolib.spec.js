/// <reference types="cypress" />

describe("Covid", () => {
  beforeEach(() => {
    cy.visit("https://www.doctolib.fr/");

    cy.window().then((win) => {
      const script = win.document.createElement("script");
      script.src =
        "https://www.gstatic.com/dialogflow-console/fast/messenger/bootstrap.js?v=1";
      win.document.body.appendChild(script);

      const div = win.document.createElement("div");
      div.innerHTML =
        '<df-messenger intent="WELCOME" chat-title="doctolib" agent-id="57ed3011-798b-4601-ba7a-3a5227e7daae" language-code="fr"  ></df-messenger>';
      div.id = "chatbot";
      win.document.body.appendChild(div);
    });

    cy.get("#didomi-notice-disagree-button").click();

    cy.get(".message-list-wrapper", { includeShadowDom: true }).should("exist");
    cy.get(".message-list-wrapper", { includeShadowDom: true }).click();
  });

  it("exists", () => {
    cy.get(".message-list-wrapper", { includeShadowDom: true })
      .contains("Covid")
      .should("exist");
  });

  it("has quick 4 quick replies", () => {
    cy.get(".message-list-wrapper", { includeShadowDom: true })
      .contains("Covid")
      .click();
    cy.get(".df-chips-wrapper", { includeShadowDom: true })
      .children()
      .should("have.length", 4);
  });

  it("has a quick reply that return to home", () => {
    cy.get(".message-list-wrapper", { includeShadowDom: true })
      .contains("Covid")
      .click();
    cy.get(".message-list-wrapper", { includeShadowDom: true })
      .contains("Je ne veux pas plus d'informations à propos de la Covid-19")
      .click();
    cy.get(".message-list-wrapper", { includeShadowDom: true })
      .not(".clicked")
      .contains("Covid")
      .should("exist");
  });
});
